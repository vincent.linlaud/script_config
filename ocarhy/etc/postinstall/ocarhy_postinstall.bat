call %OSGEO4W_ROOT%\bin\o4w_env.bat
call %OSGEO4W_ROOT%\bin\py3_env.bat
python %OSGEO4W_ROOT%\apps\ocarhy\prepare.py
:: install shortcuts on desktop
nircmd shortcut "%OSGEO4W_ROOT%\apps\ocarhy\ocarhy_launch_bureau.bat" "~$folder.desktop$" "OCARHY" "%OSGEO4W_ROOT%\apps\ocarhy\OCARHY.qgz" "%OSGEO4W_ROOT%\apps\ocarhy\iconeb.ico" "" "min"
nircmd shortcut "%OSGEO4W_ROOT%\apps\ocarhy\ocarhy_launch_portable.bat" "~$folder.desktop$" "OCARHY Portable" "%OSGEO4W_ROOT%\apps\ocarhy\OCARHY_PORTABLE.qgz" "%OSGEO4W_ROOT%\apps\ocarhy\iconep.ico" "" "min"
nircmd shortcut "%OSGEO4W_ROOT%\apps\ocarhy\qgis_launch.bat" "~$folder.desktop$" "QGIS" "" "%OSGEO4W_ROOT%\apps\ocarhy\iconeqgis.ico" "" "min"