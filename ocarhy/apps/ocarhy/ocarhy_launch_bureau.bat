
@echo off

set ocarhy=%~dp0
set osgeo_root=%ocarhy:apps\ocarhy=%
call %osgeo_root%\bin\o4w_env.bat
call %osgeo_root%\bin\py3_env.bat

start "QGIS" /B "%OSGEO4W_ROOT%\bin\qgis-ltr-bin.exe" --profile Bureau %1

