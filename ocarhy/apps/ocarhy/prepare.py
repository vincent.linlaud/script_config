import os
import platform
import shutil
from configparser import ConfigParser
from os import getenv
from pathlib import Path

# profiles location
PATH_TO_INIS = Path(os.path.join(os.path.dirname(__file__)))
# PATH_TO_INIS = Path(getenv("OSGEO4W_ROOT", "C:\\OSGeo4W64"), "apps", "qgis-si")
PATH_TO_PROFILES = Path(getenv(
                            "APPDATA"),
                            os.path.join(os.environ['HOMEDRIVE']+os.environ["HOMEPATH"],
                                        "AppData","Roaming")
                                        ,"QGIS","QGIS3",'profiles')


def local_dir(path):
    return os.path.join(PATH_TO_INIS,path)


def deploy():
    if os.path.exists(os.path.join(PATH_TO_PROFILES,'Bureau')):
        shutil.rmtree(os.path.join(PATH_TO_PROFILES,'Bureau'))
    if os.path.exists(os.path.join(PATH_TO_PROFILES,'Portable')):
        shutil.rmtree(os.path.join(PATH_TO_PROFILES,'Portable'))

    shutil.copytree(local_dir('profiles/Bureau'),os.path.join(PATH_TO_PROFILES,'Bureau'))
    shutil.copytree(local_dir('profiles/Portable'),os.path.join(PATH_TO_PROFILES,'Portable'))

def prepare_files():
    ocarhy_DIR = os.path.dirname(os.path.realpath(__file__))

    for profile in ['Bureau','Portable']:
        base_DIR = os.path.join(ocarhy_DIR,"profiles", profile, 'QGIS')
        INI_FILE = os.path.join(base_DIR, 'QGISCUSTOMIZATION3.ini')
        # splash screen folder
        SPLASH_OFFICE_DIR = os.path.join(base_DIR,'splash') # à régler avec ton dossier contenant le splash.png, '..' permet de revenir au dossier parent 
        SPLASH_OFFICE_DIR = SPLASH_OFFICE_DIR.replace(os.sep,'/')+'/'

        lines = []
        with open(INI_FILE) as f:
            for line in f:
                tag = '{{ SPLASH_DIR }}'
                if tag in line:
                    line = line.replace(tag, SPLASH_OFFICE_DIR)
                lines.append(line)

        with open(INI_FILE, 'w') as f:
            for line in lines:
                f.write(line)

        #######################################################
        # On rajoute la gestion de nos chemins pour le plugin #
        #######################################################

if __name__ == "__main__":
    prepare_files()
    deploy()