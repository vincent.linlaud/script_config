**L’outil OCARHY**

OCARHY est un outil SIG mutualisé pour la gestion des milieux aquatiques. Il est a destination de toutes structures GEMAPI souhaitant améliorer l’exploitation de leurs données sur leur territoire. OCARHY est adressé à un public. L’objectif de l’outil est de permettre aux utilisateurs de pouvoir saisir de la donnée relative aux milieux aquatiques. De pouvoir accéder à un large catalogue de données et de pouvoir extraire ces données pour produire rapidement des diagrammes ou des cartes. 

**Le contenu de la solution**

La solution est une base de données qui mutualise de nombreuses ressources produites par des acteurs nationaux comme l’IGN, la SANDRE ou encore l’INPN. Celle-ci contient aussi des données en lien avec les besoins métiers des techniciens, des tables pour réaliser de la saisie de diagnostic ou du suivi des actions de gestion. Cette base est paramétrée avec des triggers, des contraintes ainsi que des droits d’utilisateurs limitants la saisie des techniciens uniquement à leur territoire d’actions.

Pour interagir avec la base, le logiciel QGIS est exploité. Son interface a été customisée et un projet personnalisé a été créé. Pour réaliser une saisie de donnée, des formulaires ont été mis en place pour simplifier la saisie de chaque donnée. Enfin, grâce à l'extension DataPlotly, il est possible de mettre en place des bilans comportant des graphique et des cartes dynamiques en fonction d'une entité géographique sélectionnée. 

**L’installation et l’utilisation**

Pour installer l'outil et l'utiliser, vous pouvez suivre les indications dans le **Guide utilisateur**
Pour comprendre comment l'outil a été développé vous pouvez vous référer au **Guide Administrateur**. Ce dernier explique les différentes étapes pour gérer la base de données et administrer les paquets d'installation.
